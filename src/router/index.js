
import Vue from 'vue'
import Router from 'vue-router'
import Login from '../views/Login'
import Home from '../views/Home'
import store from '@/store'
import Users from '@/views/users/Users'




Vue.use(Router)


const routes=new Router({
  routes:[{
    path:'/',
    redirect:'/login'
  },
  {
    path:'/login',
    name:'login',
    component:Login,
    meta:{requiresAuth: true}
  },
  {
    path:"/home",
    name:'home',
    component:Home,
    children:[{
        path:'users',
        name:'users',
        component:Users,
    }]
  }]
})
export default routes