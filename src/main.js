// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from './http'


Vue.config.productionTip = false
// 设置为 false 以阻止 vue 在启动时生成生产提示。

// import 'reset-css';
Vue.use(ElementUI);
Vue.prototype.$http=axios;


/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
